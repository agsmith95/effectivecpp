// g++ -std=c++14 type_name.hpp item1_template_type_deduction.cpp

#include <iostream>
#include <algorithm>

#include "../defines.h"
#include "type_name.hpp"

// < Case 1 : param type is a ref or ptr, but not a universal ref
template <typename T>
void f(T& param) {
    std::cout << "T/param type: " 
              << type_name<T>() << '/' 
              << type_name<decltype(param)>() << '\n';
}

template <typename T>
void cf(const T& param) {
    std::cout << "T/param type: " 
              << type_name<T>() << '/' 
              << type_name<decltype(param)>() << '\n';
}

template <typename T>
void pf(T* param) {
    std::cout << "T/param type: " 
              << type_name<T>() << '/' 
              << type_name<decltype(param)>() << '\n';
}
// />

// < Case 2: ParamType is a Universal Reference
template <typename T>
void urf(T&& param) {
    std::cout << "T/param type: " 
              << type_name<T>() << '/' 
              << type_name<decltype(param)>() << '\n';
}
// />

// < Case 3: ParamType is Neither a Pointer nor a Reference
template <typename T>
void vf(T param) {
    std::cout << "T/param type: " 
              << type_name<T>() << '/' 
              << type_name<decltype(param)>() << '\n';
}
// />

// < Array Arguments
template<typename T, std::size_t N>
constexpr std::size_t arrSize(T (&)[N]) noexcept
{
    return N;
}
// />

// < Function Pointers
void foo(int, double) {};   // had to define for the linker
                            // simple declaration caused linkage error
// >

int main() {
    int        x   = 0;
    const int  cx  = 0;
    const int &crx = 0;
    const int *cpx = &x;
    
    // < Case 1 : param type is a ref or ptr, but not a universal ref
    f(x);       // int/int&
    f(cx);      // const int/const int&
    f(crx);     // const int/const int&
    NEW_LINE
    cf(x);      // int/const int&
    cf(cx);     // int/const int&
    cf(crx);    // int/const int&
    NEW_LINE
    pf(&x);     // int/int*
    pf(cpx);    // const int/int const*
    NEW_LINE
    // />

    // < Case 2: ParamType is a Universal Reference
    urf(x);     // int&/int&
    urf(cx);    // const int&/const int&
    urf(crx);   // const int&/const int&
    urf(270);   // int/int&&
    urf(cpx);   // int const*&/int const*&
    NEW_LINE
    // />

    // < Case 3: ParamType is Neither a Pointer nor a Reference
    vf(x);     // int/int
    vf(cx);    // int/int
    vf(crx);   // int/int
    NEW_LINE
    // />

    // < Array Arguments
    const char  array[] = "hello world!";
    const char* ptrToStr = array; // array decays to pointer

    vf(array);  // char const*/char const*
    f(array);   // const char [13]/const char [13]&
    std::cout << "array[] size = " << arrSize(array) << '\n';
    NEW_LINE

    vf(ptrToStr);   // char const*/char const*
    f(ptrToStr);    // char const*/char const*&
    NEW_LINE
    // />

    // < Function Pointers
    vf(foo);    // void (*)(int, double)/void (*)(int, double)
    f(foo);     // void (int, double)/void (int, double)&
    NEW_LINE
    // />

    return 0;
}
